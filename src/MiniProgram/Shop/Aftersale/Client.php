<?php

namespace EasyWeChat\MiniProgram\Shop\Aftersale;

use EasyWeChat\Kernel\BaseClient;

/**
 * 自定义版交易组件及开放接口 - 售后接口
 *
 * @package EasyWeChat\MiniProgram\Shop\Aftersale
 * @author HaoLiang <haoliang@qiyuankeji.cn>
 */
class Client extends BaseClient
{
    /**
     * 创建售后
     *
     * @param array $aftersale
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function add(array $aftersale)
    {
        return $this->httpPostJson('shop/ecaftersale/add', $aftersale);
    }

    /**
     * 获取订单下售后单
     *
     * @param array $order 订单数据
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(array $order)
    {
        return $this->httpPostJson('shop/ecaftersale/get', $order);
    }

    /**
     * 更新售后
     *
     * @param array $order 订单数据
     * @param array $aftersale 售后数据
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(array $order, array $aftersale = [])
    {
        return $this->httpPostJson('shop/ecaftersale/update', array_merge($order, $aftersale));
    }

    /**
     * 商家更新售后单
     *
     * @param array $salesOrder
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function merchantUpdate(array $salesOrder)
    {
        return $this->httpPostJson('shop/ecaftersale/merchantupdate', $salesOrder);
    }

    /**
     * 用户取消售后申请
     *
     * @param array $aftersale
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(array $aftersale)
    {
        return $this->httpPostJson('shop/ecaftersale/cancel', $aftersale);
    }

    /**
     * 用户上传退货物流
     *
     * @param array $aftersale
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadReturnInfo(array $aftersale)
    {
        return $this->httpPostJson('shop/ecaftersale/uploadreturninfo', $aftersale);
    }

    /**
     * 商家同意退货
     * @param array $data
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function acceptReturn(array $data)
    {
        return $this->httpPostJson('shop/ecaftersale/acceptreturn', $data);
    }

    /**
     * 商家同意退款
     *
     * @param array $data
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function acceptRefund(array $data)
    {
        return $this->httpPostJson('shop/ecaftersale/acceptrefund', $data);
    }

    /**
     * 商家拒绝售后
     *
     * @param $aftersale_id
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function reject($aftersale_id)
    {
        return $this->httpPostJson('shop/ecaftersale/reject', ['aftersale_id' => $aftersale_id]);
    }

    /**
     * 商家上传退款凭证
     *
     * @param array $data
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadCertificates(array $data)
    {
        return $this->httpPostJson('shop/ecaftersale/upload_certificates', $data);
    }

    /**
     * 商家更新订单售后期
     *
     * @param array $data
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateDeadline(array $data)
    {
        return $this->httpPostJson('shop/ecaftersale/upload_certificates', $data);
    }

    /**
     * 获取售后单列表
     *
     * @param array $data
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getList(array $data)
    {
        return $this->httpPostJson('shop/ecaftersale/get_list', $data);
    }
}   
